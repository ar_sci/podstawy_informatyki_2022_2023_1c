## Rejestracja w serwisie [gitlab](https://gitlab.com/)

1. Wejdź na stronę serwisu [gitlab](https://gitlab.com/).
2. W prawym, górnym rogu znajduje się przycisk `Register`. Proszę kliknąć w niego lewym przyciskiem myszy.
3. Przeglądarka przeniesie ciebie do formularzu rejestracyjnego. Wypełniasz następujące dane:
    - **NIE MUSISZ podawać prawdziwych danych osobowych oraz maila**
    - imię `(First name)`, nazwisko `(Last name)`
    - nazwa użytkownika `(Username)`
    - adres mailowy `(Email)`
    - hasło `(Password)`
    - pierwszy checkbox musi zostać zaznaczony `(I accept the Terms of Service and Privacy Policy)`
    - może się pojawić reCAPTCHA, więc musisz poprawnie przejść przez weryfikacje

    ![](assets/gitlab_register.gif)

4. Po wypełnieniu formularza, naciskasz przycisk `Register`
5. Następnie serwis zapyta się czym aktualnie się zajmujesz.
    - `Role` <= wybierasz opcję: `other`, ponieważ nie ma opcji student lub uczeń
    -  `Who will be using GitLab?` <= wybierasz opcję: `Just me`, ponieważ aktualnie nie wykorzystujesz gitlaba w celach komercyjnych

    ![](assets/gitlab_role.png)

6. Koniec
