## Dodawanie klucza ssh w serwisie [gitlab](https://gitlab.com/)

1. Zaloguj się do serwistu `gitlab`
1. Po poprawnym zalogowaniu się do serwisu, kliknij w prawym górnym rogu na przycisk.
1. Wybierz opcję `Preferences`.
1. Po lewej stronie znajduje się menu, znajdź zakładkę, która nazywa się `SSH Keys`.
1. Znajdź publiczny klucz ssh na dysku (`id_rsa.pub`).
1. Otwórz plik w dowolnym edytorze tekstowym.
1. Skopiuj całą zawartość za pomocą opcji `Kopiuj`, przejdź ponownie do przeglądarki ze stroną gitlabową.
1. Wklej publiczny klucz do sekcji `Key`. \
    ![](assets/gitlab_sshaddkey.PNG)
1. Kliknij przycisk `Add key`.
1. Koniec