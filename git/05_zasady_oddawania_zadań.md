# Realizacja zadań

1. Otwórz swój katalog zawierający twoje repozytorium.
1. Upewnij się, że jesteś na `master` lub `main`.
1. Utwórz nowy branch `sci_{literki_przedmiotu}_task_{nr}` \
    np. `sci_pi_task_01`
1. Utwórz nowy katalog, o takiej samej nazwie jak twój branch
1. Zacommituj swoje zmiany
1. Wrzuć branch na serwer `git push` \
    za pierwszym razem, będziesz musiał ustawić `upstream` \
    komenda zostanie wyświetlona, którą musisz przekopiować
1. Utwórz merge request

---
## Forma w obrazkach

![](assets/homework_ss_0.png)
![](assets/homework_ss_1.png)
![](assets/homework_ss_2.png)
![](assets/homework_ss_3.png)
![](assets/homework_ss_4.png)
![](assets/homework_ss_5.png)
![](assets/homework_ss_6.png)
![](assets/homework_ss_7.png)
![](assets/homework_ss_8.png)
