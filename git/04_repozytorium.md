## Stworzenie nowego repozytorium

1. W lewej górnej cześci strony, znajduje się przycisk `Menu`
1. Wybierz `Projects` jako następny przycisk
1. Następnie w prawym, dolnym rogu, znajduje się opcja: `Create new project`
1. Wybierz `Create blank project`
1. Wprowadź nazwę, opis projketu.
1. Kliknij na przycisk `Create project`.
1. Koniec