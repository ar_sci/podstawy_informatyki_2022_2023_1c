**Pamiętaj, że zawsze przed komendą użyj słowa kluczowego `git`**

## Start
- `init` => inicjalizacja Gita w istniejącym katalogu
- `clone` => klonowanie istniejącego repozytorium \
    wprowadź link ssh lub http/https, aby pobrać repozytorium

[Podstawy Gita - Pierwsze repozytorium Gita](https://git-scm.com/book/pl/v2/Podstawy-Gita-Pierwsze-repozytorium-Gita#_git_basics_chapter)

## Podstawowe komendy GIT
- `status` => sprawdzanie stanu twoich plików
    - informacje o zmodyfikowanych plikach
    - informacje dotyczące plików, które można śledzić
    - podgląd zmian w poczekalni i poza nią
    - podpowiada o wykorzystywaniu innych komend przy merge konfliktach
- `add` => śledzenie nowych plików / dodawanie zmodyfikowanych plików do poczekalni
- `commit -m "{wiadomość}"` => zatwierdzanie zmian z opisem rewizji / zmiany
- `diff` => podgląd zmian, czyli co zostało zmienione
- `log` => podgląd historii rewizji
    - `-p` => dodatkowa flaga, która pokazuje zmiany
- `reset HEAD` => usuwanie dodanych plików do poczekalni
- `branch` => tworzenie nowej gałęzi
- `checkout -b {nazwa}` => tworzenie nowego brancha wraz z zmianą na tą gałąź
- `checkout {nazwa brancha}` => szybka zmiana brancha
- `checkout {nazwa pliku}` => szybkie przywrócenie zawartości pliku
- `branch {nazwa brancha}` => utworzenie nowego brancha
- `branch -D {nazwa brancha}` => usunięcie brancha o podanej nazwie
- `merge {nazwa brancha}` => scalanie gałęzi
- `rebase {nazwa brancha}` => aktualizacja brancha względem odbitego

## Zdalne repozytorium
- `push` => wrzucenie zmian z lokalnego gita na serwer
- `pull` => pobieranie zmian z serwera na lokalne repozytorium