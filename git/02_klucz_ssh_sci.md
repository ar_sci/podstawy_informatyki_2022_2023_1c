## Tworzenie klucza ssh (wersja SCI)

1. Utwórz katalog o nazwie `_git` na twoim dysku sieciowym.
2. Utwórz plik tekstowy oraz nadaj następującą nazwę: `git_start`.
3. Zmień rozszerzenie z `.txt` na `.bat`.
4. Otwórz plik w edytorze tekstowym (notepad, notepad++, visual studio code):
    - znajdź na dysku twardym lokalizację do pliku wykonywalnego (.exe), który uruchomi konsolę `Bash for Windows`.
    - poniżej znajduje się skrypt, który należy przekopiować do pliku, który został edytowany w punkcie 4:
    - `{{ WPISZ ŚCIEŻKĘ DO PLIKU EXE  }}` - całe wyrażenie musi zostać podmienione
    - np. `"C:\\Program Files\\cmder\\Cmder.exe"`
    ```bat
    set HOME=%cd%
    call {{ WPISZ ŚCIEŻKĘ DO PLIKU EXE  }}
    ```
    - zapisz plik
5. Utwórz plik o nazwie `.gitconfig` (musi to być pusty plik tekstowy).
    - uruchom go w programie do edycji tekstu
    - wprowadź następującą konfigurację
    ```
    [user]
        email = {{ email, który użyłeś w rejestracji konta gitlab }}
        name = {{ login lub twój nickname }}
    ```
    - zapisz plik
6. Utwórz katalog `.ssh`.
7. Przejdź do katalogu `.ssh`.
8. Uruchom konsolę `Bash for windows`
9. Wprowadź następujące polecenie:
    - `ssh-keygen.exe -t rsa -b 4096 -f ./id_rsa`
    - Konsola może poprosić ciebie o wprowadzenie dodatkowych parametrów, więc nic nie wpisuj, użyj tylko polecenie `ENTER` (klawiatura):
        - `Enter passphrase (empty for no passphrase):`
        - `Enter same passphrase again:`
10. Sprawdź, czy pliki znajdują się w katalogu:
    - `id_rsa`
    - `id_rsa.pub`
