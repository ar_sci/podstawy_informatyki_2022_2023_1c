## Tworzenie klucza ssh (wersja dom)

1. Uruchom konsolę `Bash for windows`
1. Wprowadź następujące polecenie:
    - `ssh-keygen.exe -t rsa -b 4096`
    - Konsola może poprosić ciebie o wprowadzenie dodatkowych parametrów, więc nic nie wpisuj, użyj tylko polecenie `ENTER` (klawiatura):
        - `Enter passphrase (empty for no passphrase):`
        - `Enter same passphrase again:`
    - utworzone pliki, znajdują się w katalogu domowym (Dysk C:, katalog Users), w katalogu `.ssh`
1. Wprowadź następujące komendy w terminalu:
    - `git config --global user.name "{{ login lub nickname }}"`
    - `git config --global user.email {{ email do konta gitlabowego }}`
