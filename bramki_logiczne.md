# Bramki logiczne
**Bramki logiczne** to narzędzia stanowiące elementy konstrukcyjne maszyn, automatów czy robotów. Komputery, z których obecnie korzystamy, wykorzystują miliony mechanizmów logicznych nazywanych bramkami cyfrowymi (lub inaczej bramkami logicznymi). Są to elementy elektroniczne, które przyjmują sygnały binarne na wejściach i zwracają wartości 1 lub 0 – prawda lub fałsz.

## Algebra Boole’a w programowaniu
Algebra Boole’a to rodzaj struktury algebraicznej, czyli zmienna boolowska lub wynik funkcji boolowskiej jest w stanie przyjmować tylko jedną z dwóch wartości – 0 lub 1. Typ boolowski stosuje się określania wyniku funkcji (prawda lub fałsz) i oznaczania zdarzeń „flagą”, a także wykorzystuje się jako warunek instrukcji warunkowej lub na przykład jako warunek wyjścia albo kontynuowania pętli.

## Układy logiczne
Nazywa się je również układami cyfrowymi. Operują one na wartościach dyskretnych. Tworzy się je na podstawie bramek logicznych, które realizują podstawowe operacje logiczne, takie jak iloczyn, negację, czy sumę.

## Nazewnictwo stanów
Poziomy napięć odpowiadają bezpośrednio wartościom logicznym z algebry Boole’a. Poziom logiczny równy zeru często oznaczany jest literką F (od angielskiego false, czyli “fałsz”) lub L (od angielskiego low – chodzi o “niski” poziom). Analogicznie oznaczany jest poziom równy 1 – często stosuje się oznaczenie literą T (od angielskiego true, czyli “prawda”) lub H (od angielskiego high – chodzi o “wysoki” poziom).

## Rodzaje bramek logicznych
### NOT
![](./assets/botland_bramka_logiczna_not.png)

#### Tablica (~)
| A | Y |
|---|---|
| 0 | 1 |
| 1 | 0 |

---

### AND
![](./assets/botland_bramka_logiczna_and.png)

#### Tablica (~)
| A | B | Y |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

---

### OR
![](./assets/botland_bramka_logiczna_or.png)

#### Tablica (~)
| A | B | Y |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

---

### XOR (EXOR)
![](./assets/botland_bramka_logiczna_xor.png)

#### Tablica (~)
| A | B | Y |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |


Źródło: [botland - Bramki logiczne – Jak to działa?](https://botland.com.pl/blog/bramki-logiczne-jak-to-dziala/)
