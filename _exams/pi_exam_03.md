```
Schemat blokowy:
1. Wartość bezwzględna liczby.
2. Porównywanie trzech liczb (a, b, c – trzy liczby) – wybór największej.
3. KASA. Algorytm ma działać, jak „kasa fiskalna”, czyli ma sumować (w pętli) zakupione
towary. Użytkownik podaje ceny dowolnej ilości towarów. Obliczanie sumy kończy się, gdy
podana zostanie liczba 0. Program wyświetla sumę końcową (całkowitą).
4. ŚREDNIA OCEN. Algorytm ma za zadanie obliczanie średniej ocen. Użytkownik na początku
podaje ilość ocen (n), których średnią będzie chciał policzyć, a następnie podaje kolejne oceny
(ocena). Wprowadzanie ocen kończy się z chwilą podania przez niego 0. Wtedy pojawia się
informacja o średniej ocen danego ucznia (średnia).
5. Dodawanie kolejnych liczb nieparzystych.
   * n - liczba naturalna, nieparzysta, na której ma być koniec obliczeń
   * s - suma kolejnych liczb nieparzystych aż do n-tej
   * i – kolejna liczba nieparzysta - licznik (1, 3, 5, ..., n)
   czyli jak n = 5, to wynik wynosi 1 + 3 + 5 = 9 = s
```

---

## Zadanie 6.
![](./assets/pi_exam_03_0.PNG)
```
Dane:
n = 3
n = 4
n = 5
```

---

## Zadanie 7.
![](./assets/pi_exam_03_1.PNG)
```
Dane:
a = 3
b = 8
```
