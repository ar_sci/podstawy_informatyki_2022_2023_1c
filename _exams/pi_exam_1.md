***
![](./assets/pi_exam_00.PNG)
***
Zamień z DEC na BIN, a BIN na HEX:
1. 147
1. 214
1. 138
1. 53
1. 231

Zamień z HEX na BIN:
1. 0xDEADC0DE
1. 0xBAADF00D
1. 0xC00010FF
1. 0xFEEDBABE
1. 0xFFBADD11
***
- A: 1
- B: 0
- C: 1
- D: 1

![](./assets/pi_exam_01.png)
***
- a0: 0
- a1: 1
- d0: 1
- d1: 0
- d2: 0
- d3: 1

![](./assets/pi_exam_02.png)
***
- A: 0
- B: 0
- C: 1
- D: 1

![](./assets/pi_exam_03.png)
***
