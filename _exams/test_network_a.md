# Test wiedzy - 7 pytań

1. W firmowej sieci bezprzewodowej została uruchomiona usługa polegająca na tłumaczeniu nazw mnemonicznych. Jest to usługa:
    - A => RADIUS
    - B => DNS
    - C => RDS
    - D => DHCP
1. Przydzielaniem adresów IP w sieci zajmuje się serwer:
    - A => DNS
    - B => DHCP
    - C => NMP
    - D => NetBIOS
1. Które urządzenie sieciowe pozwala połączyć sieć LAN z WAN?
    - A => Repeater
    - B => Hub
    - C => Switch
    - D => Router
1. Architektura fizyczna sieci, zwana inaczej topologią fizyczną sieci komputerowych określa:
    - A => standardy komunikacyjne sieci komputerowych 
    - B => sposób połączenia ze sobą komputerów 
    - C => przekaz informacji pomiędzy protokołami sieciowymi modelu OSI 
    - D => wzajemną komunikację komputerów pomiędzy sobą
1. Które stwierdzenie dotyczące protokołu DHCP jest prawidłowe? 
    - A => Jest to protokół dostępu do bazy danych 
    - B => Jest to protokół konfiguracji hosta 
    - C => Jest to protokół routingu
    - D => Jest to protokół przesyłania plików
1. Urządzenie w sieci komputerowej któremu przypisano adres IP to:
    - A => Klient
    - B => Router
    - C => Serwer
    - D => Host
1. Wiele strumieni światła w jednym rdzeniu to cecha jakiego rodzaju medium transmisyjnego?
    - A => WiFi
    - B => Światłowodu wielomodowego
    - C => Światłowodu jednomodowego
    - D => Koncentryka
