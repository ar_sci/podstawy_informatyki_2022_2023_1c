```
1. Wczytuje dwie liczby. Oblicza iloczyn. Wypisuje wynik na konsoli aplikacji.
2. Napisz program, który oblicza objętość kuli o promieniu r. Wartość promienia wprowadzamy z klawiatury. W programie należy przyjąć, że r oraz wynik jest typu rzeczywistego.
   Wzór na kulę: 4/3 * pi * r^3;
   PI: 3.14159
3. Wczytuje dwie liczby. Rozpoznaje która jest większa, i wypisuje w kolejności: większa, mniejsza. Jeżeli są równe wyświetla komunikat Liczby są równe.
4. Wczytuje dwie liczby. Oblicza sumę i różnicę. W zależności od wyników pisze suma większa od różnicy, suma mniejsza od różnicy, lub suma jest równa różnicy.
5. Gra: Kamień, Papier, Nożyce.
```
