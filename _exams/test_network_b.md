# Test wiedzy - 7 pytań

1. Z ilu par żył składa się kabel typu skrętka:
    - A => 8
    - B => 2
    - C => 4
    - D => 6
1. Zapis bps oznacza:
    - A => Ilość bitów przesłanych w jednostce czasu 
    - B => Stosunek wielkości pliku do czasu jego przesłania 
    - C => Wielkość pliku przesłanego przez sieć 
    - D => Jednostkę danych stosowaną do określania wielkości dysków twardych
1. Reguły i zasady komunikacji w sieci określają:
    - A => Protokoły komunikacyjne
    - B => Normy prawne
    - C => Producenci urządzeń sieciowych
    - D => Administratorzy sieci
1. W architekturze sieci lokalnych typu klient-serwer:
    - A => każdy komputer zarówno udostępnia jak i korzysta z zasobów innych komputerów
    - B => wszystkie komputery klienckie mają dostęp do zasobów komputerowych
    - C => wyróżnione komputery pełnią rolę serwerów udostępniających zasoby, a pozostałe komputery z tych zasobów korzystają
    - D => żaden z komputerów nie pełni roli nadrzędnej w stosunku do pozostałych
1. Translacją nazw domenowych na adresy sieciowe zajmuje się usługa:
    - A => DNS
    - B => SNMP
    - C => DHCP
    - D => SMTP
1. W jakiej fizycznej topologii uszkodzenie jednej stacji roboczej zatrzyma działanie całej sieci?
    - A => Pierścienia
    - B => Magistrali
    - C => Siatki
    - D => Drzewa
1. Prywatna sieć wykorzystująca protokoły komunikacyjne takie jak sieć Internet to 
    - A => Extranet
    - B => LAN
    - C => Intranet
    - D => WAN
