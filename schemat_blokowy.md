# Schemat blokowy

**Schemat blokowy** \
jest graficznym przedstawieniem algorytmu. Zawiera operacje które mają być wykonywane, wzajemne powiązania między nimi, oraz kolejność wykonywania poszczególnych operacji.  W schemacie blokowym poszczególne operacje przedstawione są za pomocą odpowiednio połączonych bloków. Połączenia określają kolejność i sposób wykonywania operacji realizujących dany algorytm. Schematy blokowe pozwalają na prostą zamianę instrukcji na instrukcje programu komputerowego. 

## Podstawowe elementy budowy
![](assets/schemat_blokowy.jpg)

## Zasady budowania schematu blokowego
1. Schemat blokowy składa się z bloków połączonych zorientowanymi liniami, określającymi kolejność wykonywania poszczególnych czynności.
2. Z każdego bloku może wychodzić tylko jedna strzałka. Wyjątkiem jest blok decyzyjny – z którego muszą wychodzić dwie strzałki: Tak oraz Nie.
3. Do każdego bloku może dochodzić dowolna liczba strzałek. Każda z nich oznacza wykonanie wszystkich czynności danego bloku. \
*Według innej zasady, do każdego bloku może dochodzić tylko jedna strzałka. W takim przypadku kilka strzałek dochodzi do punktu koncentracji, od którego jedna strzałka prowadzi do bloku.*
4. Wszystkie bloki muszą mieć co najmniej jedną strzałkę dochodzącą i wychodzącą. Wyjątkami są: łączniki i bloki graniczne **START** i **KONIEC**.
5. Strzałki mogą się łączyć ale nie mogą się rozdzielać.
6. Każdy schemat blokowy musi zawierać dokładnie jeden blok graniczny **START** i przynajmniej jeden blok graniczny **KONIEC**.

## Przykłady:
![](assets/sb_liniowy.jpg)
![](assets/sb_warunek.jpg)
![](assets/sb_iteracja.jpg)

****
### Źródła:
- [Schemat blokowy](http://tiked.pl/2019/11/21/schemat-blokowy/)
