# System binarny: zadania
## Zadanie jest obowiązkowe

## Zadania
- Zadanie_0: Zamien liczby z systemu 10 na liczby systemu 2:
    - 13107
    - 136
    - 2321
    - 101
    - 2702

- Zadanie_1: Zamien liczby binarne na szesnastkowe:
    - 0110 1011 0111 1110
    - 1010 0011 0110 1011
    - 0010 0101 1111 0001

- Zadanie_2: Wykonaj obliczenia:
    ```
     0011 1101
    +0001 1110
    ----------
    ```
    ```
     0011 1110
    *0010 1001
    ----------
    ```
    ```
     1110 1001
    &1001 0111
    ----------
    ```
    ```
     1110 1101
    |1001 0010
    ----------
    ```
    ```
    1101 1001
    ^1001 0111
    ----------
    ```

